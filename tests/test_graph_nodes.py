import pytest

from jlvandenhout.graph import Graph


def test_add_and_remove():
    graph = Graph()
    graph.nodes.add("node")
    assert "node" in graph.nodes

    graph.nodes.remove("node")
    assert "node" not in graph.nodes

    graph.edges.set(1, 2)
    graph.nodes.remove(2)
    assert (1, 2) not in ((p, s) for p, s in graph.edges)


def test_get_adjacent():
    graph = Graph()
    graph.edges.set(1, 2)
    assert 2 in graph.nodes.after(1)
    assert 1 in graph.nodes.before(2)
    assert 1 not in graph.nodes.after(2)
    assert 2 not in graph.nodes.before(1)

    graph.edges.set(1, 3)
    graph.edges.set(2, 3)
    assert set(graph.nodes.after(1)) == {2, 3}
    assert set(graph.nodes.before(3)) == {1, 2}
