import pytest

from jlvandenhout.graph import Graph


def test_closure():
    graph = Graph()
    graph.edges.set(1, 2)
    graph.edges.set(2, 3)
    graph.edges.set(1, 4)

    assert set(graph.closure) == {3, 4}
