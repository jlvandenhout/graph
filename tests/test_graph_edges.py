import pytest

from jlvandenhout.graph import Graph


def test_add_and_remove():
    graph = Graph()
    graph.edges.set(1, 2)
    assert 1 in graph.nodes and 2 in graph.nodes
    assert (1, 2) in set((p, s) for p, s in graph.edges)
    assert (2, 1) not in set((p, s) for p, s in graph.edges)

    graph.edges.set(2, 1)
    assert (2, 1) in set((p, s) for p, s in graph.edges)

    graph.edges.unset(2, 1)
    assert (2, 1) not in set((p, s) for p, s in graph.edges)


def test_get_adjacent():
    graph = Graph()
    graph.edges.set(1, 2)
    assert (1, 2) in set((p, s) for p, s in graph.edges.after(1))
    assert (1, 2) in set((p, s) for p, s in graph.edges.before(2))
    assert (2, 1) not in set((p, s) for p, s in graph.edges.after(2))
    assert (2, 1) not in set((p, s) for p, s in graph.edges.before(1))


def test_set_and_get_edge_values():
    graph = Graph()
    graph.edges.set(1, 2)
    assert graph.edges.get(1, 2) is None

    obj = object()
    graph.edges.set(1, 2, obj)
    assert graph.edges.get(1, 2) == obj
    assert set(edge.get() for edge in graph.edges) == {obj}

    for edge in graph.edges:
        edge.set(0)
    assert set(edge.get() for edge in graph.edges) == {0}
