import pytest

from jlvandenhout.graph import Graph
from jlvandenhout.graph import search


def test_breadth_first_spanning_tree():
    graph = Graph()
    graph.edges.set(1, 2)
    graph.edges.set(3, 4)
    graph.edges.set(2, 4)
    graph.edges.set(4, 5, True)

    follow = lambda e: e.get() is None

    subgraph = search(graph, [3, 1], follow)
    assert 2 in subgraph.nodes.after(1)
    assert 4 in subgraph.nodes.after(3)
    assert 4 not in subgraph.nodes.after(2)
    assert 5 not in subgraph.nodes.after(4)


def test_depth_first_spanning_tree():
    graph = Graph()
    graph.edges.set(1, 2)
    graph.edges.set(3, 4)
    graph.edges.set(2, 4)
    graph.edges.set(4, 5, True)

    follow = lambda e: e.get() is None

    subgraph = search(graph, [3, 1], follow, True)
    assert 2 in subgraph.nodes.after(1)
    assert 4 in subgraph.nodes.after(2)
    assert 4 not in subgraph.nodes.after(3)
    assert 5 not in subgraph.nodes.after(4)
