## Contributing
Help is always welcome and much appreciated. If you find a bug, have an idea for an improvement or feature and want to contribute yourself, feel free to follow this document.

Please start by following the development environment setup, so you have a working clone of the repository. After that you can start with opening an issue describing your contribution. When you have a plan for your contribution, create a branch from your issue and start writing. Once you have a working solution, create a merge request to review your solution and get it merged.

---
- [1. Setup](#1-setup)
  - [1.1. Fork and clone the repository](#11-fork-and-clone-the-repository)
  - [1.2. Create a virtual environment](#12-create-a-virtual-environment)
  - [1.3. Install and test](#13-install-and-test)
- [2. Start contributing](#2-start-contributing)
  - [2.1. Open an issue](#21-open-an-issue)
  - [2.2. Create a branch for your issue and start coding](#22-create-a-branch-for-your-issue-and-start-coding)
  - [2.3. Create a merge request](#23-create-a-merge-request)
- [3. Release to production](#3-release-to-production)
  - [3.1. Update the changelog and version](#31-update-the-changelog-and-version)
  - [3.2. Tag the release commit](#32-tag-the-release-commit)
---


### 1. Setup
#### 1.1. Fork and clone the repository
First of all you need to clone the repository to your development machine. To do this, most IDE's provide git integration, but if you need to do it from the command line you have to install [git](https://git-scm.com/downloads). Then run the following command:

```
git clone {repository-url}
```

#### 1.2. Create a virtual environment
It is good practice to use a virtual environment for Python projects. This way you will not mess up your global Python installation and keep installed packages contained in the environment. If you are done with the environment or anything goes wrong, you just delete it and start over.

You can use the built-in [venv](https://docs.python.org/3/library/venv.html) package to create environments, but using a wrapper makes things much easier. Using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io) for example, would only require you to once set two environment variables: WORKON_HOME and PROJECT_HOME. If you have cloned the repository inside PROJECT_HOME, creating a virtual environment would then be as easy as:

```
mkproject {project-name}
```

You can then use `workon` and `deactivate` commands to start or stop using the environment.

#### 1.3. Install and test
Now it is time to install the package for development and test if everything works correctly. To do this you will need to install the package itself in develpment mode and a package called [pytest](https://pytest.org). To install and test, run the following commands:

```
pip install --editable .
pip install pytest
pytest
```

If all goes well, you should now see the output of the tests. You now have a fully functional development environment.

### 2. Start contributing
To keep development organized, this project adopts the [GitLab Flow](https://docs.gitlab.com/ee/university/training/gitlab_flow.html#feature-branches) using feature branches. The resulting steps are:

1. [Always start with an issue](https://about.gitlab.com/2016/03/03/start-with-an-issue/) for discussion and code planning.
2. Create a branch from the issue to start commiting and pushing code.
3. Create a merge request for review and get your merge request accepted.

#### 2.1. Open an issue
The first thing to do when you have an idea is opening an issue in the issue tracker of the project. This way you notify the community about your idea and allow others to help you with it. This is the planning phase where you support your idea and start planning the outline of your code.

#### 2.2. Create a branch for your issue and start coding
When you have discussed your idea and planned your code, it is time to start coding. Create a branch for your issue, check out the branch in your local repository and start coding.

> Commit and test frequently with titles in the imperative mood, as if you were finishing the following sentence: This commit will...

If git is not integrated in your IDE, use the following commands to stage files and commit the staged files:

```
git add {file}
git commit -m "{message}"
```

To maintain a consistant and high quality repository, running some tools over your code is essential. Currently this project uses [black](https://black.readthedocs.io) for formatting and [pylint](https://pylint.readthedocs.io) for linting (with trailing spaces disabled). Install both and run them on your file or directory containing the changes:

```
pip install black pylint
black {path-to-file-or-directory}
pylint --disable=C0303 {path-to-file-or-directory}
```

> Black will correct the files in place, but pylint only reports issues it finds. Take the time to correct the issues it finds. If you really can't change the issue, discuss settings of pylint in the issue tracker, but only if it is really necessary.

Once you have implemented your idea and checked for consistency, run the tests to make sure nothing broke:

```
pytest
```

If all is well, push your code to the remote repository. This will start a CI pipeline on GitLab to check consistency and run tests.

```
git push origin {branch-name}
```

#### 2.3. Create a merge request
To get your idea merged, create a merge request to start reviewing your code. While discussing your code with the maintainers, you can still commit updates to the branch when necessary.

> If your implementation is not ready yet to be merged, but you need advice, you can also create a merge request. In this case, start the merge request title with `WIP:`, meaning the merge request is still work in progress. When you are ready for merging, remove `WIP:` from the title.

### 3. Release to production
When the package has to be released, update the changelog and version and tag the commit with a release note containing the latest changes. This will trigger a release to PyPi.

#### 3.1. Update the changelog and version
The changelog will contain an `[Unreleased]` section where changes regarding usage of the package are logged. Change the `[Unreleased]` tag to the version to be released according to [Semantic Versioning](https://semver.org) and append the date of release in the YYYY-MM-DD format. For example:

```
[1.0.4] - 2019-07-24
```

Then update the version in [the setup file](setup.py) to the same version number.

#### 3.2. Tag the release commit
Commit the above changes and tag that commit. The title should contain only the version number. The description should read:

```
Release {version}
```

The release notes should contain the logged changes under the corresponding section in [the changelog](CHANGELOG.md)