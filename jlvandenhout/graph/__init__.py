"""Create and manipulate a graph."""

from .algorithms import search
from .core import Graph
