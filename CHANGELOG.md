## Changelog
All usage related changes to this project will be logged in this file.

> Based on [Keep a Changelog](https://keepachangelog.com) and [Semantic Versioning](https://semver.org).

### [2.1.0] - 2019-08-15
#### Changed
- Graph has the focus of the interface, nodes and edges are properties.
- Iteration is now over nodes and edges, not the graph itself.
- Edge is a pure convenience proxy class.

#### Removed
- Removed length dunders in favor of explicit graph properties.
- Reverse is feature creep for now.

### [2.0.0] - 2019-08-13
#### Changed
- Edge to a pure proxy class.

#### Removed
- Redundant add method from Edge class.

### [1.2.0] - 2019-08-12
#### Added
- Ability to calculate the closure set of a graph.

### [1.1.0] - 2019-08-12
#### Added
- Graph elements are now sized.

### [1.0.0] - 2019-08-08
#### Added
- Get, set and unset methods related to edge value operations.
- Graph interface to all elements, to get back to the graph itself.
- Reverse functionality according #13.

#### Changed
- Nodes add method to only accept one node.
- Add and remove methods to be related to node and edge instances.

#### Removed
- Unnecessary error definitions.

### [0.4.0] - 2019-07-31
#### Changed
- The search algorithm to just follow an edge or not, according #10.

### [0.3.0] - 2019-07-29
#### Added
- A search algorithm according #9.

### [0.2.0] - 2019-07-24
#### Added
- New edge functionality discussed in #8.
- New usage example reflecting the change.

### [0.1.0] - 2019-07-24
#### Added
- README and CONTRIBUTING documentation.
- A proper changelog based on [Keep a Changelog](https://keepachangelog.com).
- Proper versioning based on [Semantic Versioning](https://semver.org).
- Initial project structure and tests.